import Vue from 'vue'
import Vuelidate from "vuelidate/src";
import VueMeta from 'vue-meta'

import Paginate from 'vuejs-paginate'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import dateFilter from "./filter/date_filter";
import localizeFilter from "./filter/localize.filter";
import messagePlugin from './utils/message.plugin'
import Loader from "./components/app/Loader";
import metaTitle from "./utils/meta.title"

import 'materialize-css/dist/js/materialize.min'
import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import currencyFilter from "./filter/currency_filter";

import tooltip from "./directives/tooltip";
Vue.config.productionTip = false
Vue.use(messagePlugin)
Vue.use(Vuelidate)
Vue.use(metaTitle)
Vue.use(VueMeta, {refreshOnceOnNavigation: true});

Vue.directive('tooltip', tooltip)

Vue.filter('date', dateFilter)
Vue.filter('localize', localizeFilter)
Vue.filter('currency', currencyFilter)

Vue.component('Loader', Loader)
Vue.component('paginate', Paginate)

firebase.initializeApp({
  apiKey: "AIzaSyDbfSphgOoBt_hzzoO7SCCWtcLjqiThHJc",
  authDomain: "vueaza.firebaseapp.com",
  databaseURL: "https://vueaza.firebaseio.com",
  projectId: "vueaza",
  storageBucket: "vueaza.appspot.com",
  messagingSenderId: "325754272353",
  appId: "1:325754272353:web:0441f113277a2b41b54820",
  measurementId: "G-EEKCP2YJP9"
});

let app;

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
});
