export default {
    'logout' : 'Вы вышли из системы',
    'login' : 'Для начала войдите в систему',
    'auth/wrong-password' : 'Неверный пароль',
    "auth/user-not-found" : 'Пользователь с таким  email не существует',
    "auth/email-already-in-use" : 'Пользователь с таким  email уже существует'
}